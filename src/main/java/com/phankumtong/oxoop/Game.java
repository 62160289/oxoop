/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phankumtong.oxoop;

import java.util.Scanner;

/**
 *
 * @author ROG
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, column;
    Scanner kb = new Scanner(System.in);
    char playAgain;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");

    }

    public void showTable() {
        table.showTable();

    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            column = kb.nextInt() - 1;
            if (table.setRowCol(row, column)) {
                break;
            }

            System.out.println("Error: table at row and col is not empty!!!");
        }

    }

    public void showTurn() {
        System.out.println(table.getcurrentPlayer().getName() + "turn");

    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcome();
        while (true) {

            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWinner().getName() + "WIn!!");
                }
                System.out.println("You want Play Again?(Y/N)");
                playAgain = kb.next().charAt(0);
                if (playAgain == 'Y') {
                    this.showTable();
                } else {
                    break;
                }

                newGame();
            }
            table.switchPlayer();

        }

    }

}
